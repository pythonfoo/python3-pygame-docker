## run this command so that docker can open the socket
# xhost +

docker run -it --rm -v /tmp/.X11-unix:/tmp/.X11-unix -v $PWD:/home -e DISPLAY=unix$DISPLAY --device /dev/snd --name pygame3 olafgladis/python3-pygame python3 /home/app.py
